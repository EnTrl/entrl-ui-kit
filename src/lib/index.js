import Alert from "./alert/alert";
import Button from "./button/button";
import Card from "./card/card";
import Cards from "./cards/cards";
import CheckboxGroup from "./checkbox-group/checkbox-group";
import Div from "./container/container";
import Dropdown from "./dropdown/dropdown";
import Header from "./header/header";
import Icon from "./icon/icon";
import Input from "./input/input";
import Checkbox from "./input-controls/checkbox/checkbox";
import Radio from "./input-controls/radio/radio";
import Switch from "./input-controls/switch/switch";
import Link from "./link/link";
import Modal from "./modal/modal";
import RadioGroup from "./radio-group/radio-group";
import SwitchGroup from "./switch-group/switch-group";
import Label from "./typography/labels/labels";

export default {
	Alert: Alert,
	Button: Button,
	Card: Card,
	Cards: Cards,
	CheckboxGroup: CheckboxGroup,
	Div: Div,
	Dropdown: Dropdown,
	Header: Header,
	Icon: Icon,
	Input: Input,
	Checkbox: Checkbox,
	Radio: Radio,
	Switch: Switch,
	Link: Link,
	Modal: Modal,
	RadioGroup: RadioGroup,
	SwitchGroup: SwitchGroup,
	Label: Label
};
