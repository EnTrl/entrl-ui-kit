import React, { Component } from "react";

import "./link.css";

export default class Link extends Component {
  render() {
    const { text, id, link, active, onClick, fw, fs, fc } = this.props;
    let activeState = active ? " active" : "";
    let classNames = `${fw} ${fs} ${fc}` + activeState;

    return (
      <a id={id} href={link} className={classNames} onClick={onClick}>
        {text}
      </a>
    );
  }
}
